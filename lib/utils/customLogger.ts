//import winston from 'winston';
import {Logger, createLogger, format, transports } from 'winston';

const { combine, label, timestamp, json, printf, colorize, errors } = format ;
const config = {
    levels: {
        error: 0,
        warn: 1,
        info: 2,
        verbose: 3,
        debug: 4,
        silly: 5
    },
    colors: {
        error: 'red',
        debug: 'blue',
        warn: 'yellow',
        data: 'grey',
        info: 'green',
        verbose: 'cyan',
        silly: 'magenta',
        custom: 'yellow'
    }
} ;



const myconsole =  new transports.Console;
myconsole.name = 'console';
myconsole.level = process.env.NODE_DEBUG_LEVEL;
const createLoggerConfig = function (category: string ): Logger {
    const logger = createLogger({
        levels: config.levels,
        format: combine(
            json(),
            label({ label: `${category}` }),
            colorize(),
            errors({ stack: true }),
            // splat(),
            timestamp({
                format: 'YYYY-MM-DD HH:mm:ss'
            }),
            printf( (msg): string => {
                return `${msg.timestamp} [Postlab] ${msg.level} - ${category} : ${JSON.stringify(msg.message)}` ;
            })
        ),

        level: process.env.NODE_DEBUG_LEVEL,
        defaultMeta: { service: `${category}` },
        transports: [
            myconsole
        ]
    }) ;
    return logger ;
} ;
const mapLogger = new Map() ;


mapLogger.set('gitlab-svc', createLoggerConfig('gitlab-svc')) ;
mapLogger.set('redis-svc', createLoggerConfig('redis-svc')) ;
mapLogger.set('loadConfig-svc', createLoggerConfig('loadConfig-svc')) ;
mapLogger.set('bolt-main', createLoggerConfig('bolt-main')) ;


export default mapLogger ;

