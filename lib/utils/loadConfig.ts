import mapLogger from './customLogger';
import dotenv = require('dotenv') ;

const logger = mapLogger.get('loadConfig-svc');

export default class ConfigHandler {
    initLoggers = function (): void {
        const lgr = logger.transports.find( (transport: any) => {
            return transport.name === 'console' ;
        }) ;
        lgr.level = process.env.NODE_DEBUG_LEVEL ;
    } ;

    safeLoad = (): void => {
        const result = dotenv.config() ; // load generic
        this.initLoggers();
        if (result.error) {
            throw result.error ;
        }
    };
}

