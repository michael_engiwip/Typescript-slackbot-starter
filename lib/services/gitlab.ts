
'use strict' ;

//import  { request, RequestOptions } from 'http' ;
import  { request, RequestOptions } from 'https' ;
import mapLogger  from '../utils/customLogger' ;
const logger = mapLogger.get('gitlab-svc') ;

export  class GitlabService  {
    private jobid: number;
    constructor() { 
        this.jobid = 2;

     };

      options = {
        hostname: 'gitlab.com',
        path: process.env.GITLAB_TRIGGER_URL,
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        }
      };

    //options = new URL('https://encrypted.google.com/');
   // options = new URL(process.env.GITLAB_TRIGGER_URL);


    public triggerPipeline = async (): Promise<number> => {
            let statusCode = 404 ;
            const req = request(this.options, (res) => {
                logger.debug(`STATUS: ${res.statusCode}`); 
                statusCode = res.statusCode ; //201 Ok
                res.on('data', (chunk) => {
                    logger.debug(`BODY: ${chunk}`);
                });
                
            });
            req.on( 'error', (e) => {
                logger.error(`problem with request: ${e.message}`);
                throw e;
            });
            
            req.end();
            return statusCode ; 
        };

    };


   