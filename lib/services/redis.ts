import {ClientOpts ,  RedisClient}  from 'redis';
import { GitlabService } from './gitlab';
import { WebClient } from '@slack/web-api';
import mapLogger  from '../utils/customLogger' ;
const logger = mapLogger.get('redis-svc') ;


export  class RedisService  {
    protected gitlabsvc: GitlabService ;
    protected clientOpts: ClientOpts ;
    protected sub: RedisClient ;
    protected pub: RedisClient ;
    protected webClient: WebClient; 
    
    constructor(webcli?: WebClient){
        this.webClient = webcli;
        this.gitlabsvc = new GitlabService();
        this.clientOpts =  {} as ClientOpts ;
        this.clientOpts.host= process.env.REDIS_HOST ;
        this.clientOpts.port = parseInt(process.env.REDIS_PORT);
        this.sub = new RedisClient( this.clientOpts) ;
        this.pub = new RedisClient( this.clientOpts) ;
    }

    public publish(channel: string ,message: string): void{
        this.pub.publish(channel, message );
    }

    subscribe(channel: string): void {
        this.sub.subscribe(channel);
    } 

    /**
 *send a notif after the work is doing 
 */
    private pushNotification = async (): Promise<any> =>  {
        try {
            logger.debug('message channel');
            const result = await  this.webClient.chat.postMessage({
                token: process.env.SLACK_BOT_TOKEN,
                channel: 'master_lan',
                text: `Work is done ! 🎉 `
            });
            logger.debug(result);    
            return result ;
        }
        catch (error) {
            logger.error(error);
        }
    };
  

    //ECMAScript 6: Use arrow functions and use 'this inside callback !
    listen(): void {
        this.sub.on('message', async (channel, message) => {
            try {
                logger.debug("sub channel " + channel + ": " + message);
                const httpStatus = await this.gitlabsvc.triggerPipeline();
                //if httpStatus == 201 || 404
                const notif = await this.pushNotification() ;
            } catch (error) {
                logger.error( error);
                throw error ;
            }
        });
    
    }
    quit(): void{
        this.sub.unsubscribe();
        this.sub.quit();
        this.pub.quit();
    }
}        
   

