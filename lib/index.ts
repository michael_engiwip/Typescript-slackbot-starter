import 'module-alias/register';
import { App ,ExpressReceiver ,LogLevel } from '@slack/bolt';
import ConfigHandler from './utils/loadConfig';
import { RedisService } from './services/redis';
import mapLogger  from './utils/customLogger' ;
const logger = mapLogger.get('bolt-main') ;

const conf = new ConfigHandler();
let  rediscli: RedisService  ;
conf.safeLoad();


//Initializes express middleware for side request
const customReceiver = new ExpressReceiver({
  signingSecret: process.env.SLACK_SIGNING_SECRET
});
// Initializes your app with your bot token and signing secret
const app = new App({
  token: process.env.SLACK_BOT_TOKEN,
  signingSecret: process.env.SLACK_SIGNING_SECRET,
  logLevel: LogLevel.DEBUG,
  receiver : customReceiver
});


/***
 * root url , check if exist
 */
 customReceiver.app.get("/", (req, res) => {
  
  console.log('request / receive');
  res.send("Postlab Bot Receiver ⚡️ App is running!");
});



/**
 * listen on POST /slack/events url msg from slack server 
 * send the challenge token asked in order to validate app
 */
customReceiver.app.post("/slack/events", (req, res) => {
  logger.debug('request //slack/events receive');
  const getreq = JSON.parse(req.body);
  logger.debug('request '  +getreq );
  res.status(200).json({ "challenge": getreq.challenge });
});


/**
 * main function connect app
 */
async function connect(): Promise<any>  {
  
  try {
    await app.start(process.env.LISTEN_PORT || 3000);
     rediscli = new RedisService(app.client);
     rediscli.subscribe('gitlab-channel');   
     rediscli.listen();           
     logger.debug('⚡️ Bolt app is running!');
  } catch (e) {
    logger.error(e);
    rediscli.quit();
    process.exit(-1);


  }
}

/**
 * event url check 
 */
app.event('url_verification', async ({ event, context }) => {
  try {
  
    console.log("in url verif" );
    const result = await app.client.chat.postMessage({
      token: context.botToken,
      channel: 'id ',
      text: `Welcome to the team, <@${event.user}>! 🎉 You can introduce yourself in this channel.`
    });
    console.log(result);
  }
  catch (error) {
    console.error(error);
  }
});





// Listens for messages containing "knock knock" and responds quickly < 3s ""
app.message('deploy-knock', ({ message, say }) => {
  logger.debug("deploy-knock");
  say(`deploiement en cours ... ⚙️⛏ ⌛️`);
  rediscli.publish('gitlab-channel', 'deploy');
  
});

/**
 * Global error management
 */
app.error((error) => {
  // Check the details of the error to handle special cases (such as stopping the app or retrying the sending of a message)
  logger.error("erreur in slack bot :" + error);
});

// Listener middleware - filters out messages that have subtype 'bot_message'
const noBotMessages = function ({ message, next }): any {
  logger.debug("test-no- bot ");
  if (!message.subtype || message.subtype !== 'bot_message') {
    next();
  }
};


// The listener only receives messages from humans
app.message(noBotMessages, ({ message }) => logger.debug(
  `(MSG) User: ${message.user}
   Message: ${message.text}`
));

// React to any message that contains "happy" with a 😀
app.message('happy', async ({ message, context }) => {
  try {
    // Call the "reactions.add" Web API method
    const result = await app.client.reactions.add({
      // Use token from context
      token: context.botToken,
      name: 'grinning',
      channel: message.channel,
      timestamp: message.ts
    });
    logger.debug(result);
  } catch (error) {
    logger.error(error);
  }
});

//launch the app
connect();